let
  sources = import ./sources.nix;

  nixpkgs = import sources.nixpkgs {
    overlays = [ ];

    config = {
      allowUnfree = true;
    };
  };

in
  nixpkgs
