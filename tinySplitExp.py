
from dataclasses import dataclass

import numpy as np
import matplotlib.pyplot as plt

@dataclass
class Param:

    V: float
    J: float
    omega: float
    dt: float
    n_dt: float

    mu: float = 1.0

    @property
    def tgrid(self):
        return np.arange(self.n_dt) * self.dt

def main():

    param = Param(
        V=0.1,
        J=0.0025,
        omega=0.1025,
        dt=0.01,
        n_dt=1_000_000
    )

    J = param.J
    V = param.V
    dtV = param.dt
    dtJ = param.dt / 2

    propJ = np.array([
        [1, 0, 0],
        [0, np.cos(dtJ * J), -1.0j * np.sin(dtJ * J)],
        [0, -1.0j * np.sin(dtJ * J), np.cos(dtJ * J)],
    ], dtype=np.complex128)

    print(propJ)

    propV = np.array([
        [1, 0, 0],
        [0, np.exp(-1j * dtV * V), 0],
        [0, 0, np.exp(-1j * dtV * V)],
    ], dtype=np.complex128)

    print(propV)


    wfc = np.array([0., 1., 0.], dtype=np.complex128)
    wf_store = np.zeros((param.n_dt, 3), dtype=np.complex128)

    fullProp = propJ @ propV @ propJ

    print(fullProp)

    for i in range(param.n_dt):

        wfc = wfc @ fullProp

        wf_store[i, :] = wfc

        if i % 100 == 0:
            norm = np.real(np.abs(wfc)**2)
            print(np.real(np.abs(wfc)**2), np.sum(norm))

    # Do some plotting
    fig, ax = plt.subplots()
    ax.plot(param.tgrid, np.real(np.abs(wf_store[:, 1])**2), label="$P_1$")
    ax.plot(param.tgrid, np.real(np.abs(wf_store[:, 2])**2), label="$P_2$")

    ax.set_ylabel("Population")
    ax.set_xlabel("Time / a. u.")
    ax.legend()

    plt.show()

if __name__ == "__main__":
    main()
