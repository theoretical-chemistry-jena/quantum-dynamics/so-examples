{ pkgs ? import ./nix/pkgs.nix }:

{
  dev = with pkgs; with pkgs.python3Packages;
    mkShell {
      buildInputs = [
        which
        git

        numpy
        matplotlib
      ];
    };
}

